var express = require('express');
var router = express.Router();
var firebase = require('./sdk/firebase');
var fbRef = firebase.app().database().ref();

router.get('/register', function(req, res, next) {
    var user = {
        email: "",
        first_name: "",
        last_name: "",
        location: "",
        fav_genres: "",
        fav_artists: ""
    };
    res.render('users/register', {user: user, errors: []});
});

router.get('/login', function(req, res, next) {
    res.render('users/login');
});

router.post('/register', function(req, res, next) {
    var user = {
        email: req.body.email,
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        location: req.body.location,
        fav_genres: req.body.fav_genres,
        fav_artists: req.body.fav_artists
    };
    // Validation
    req.checkBody('first_name', 'First name is required').notEmpty();
    req.checkBody('email', 'Email is required').notEmpty();
    req.checkBody('email', 'Email is not valid').isEmail();
    req.checkBody('password', 'Password is required').notEmpty();
    req.checkBody('password2', 'Passwords do not match').equals(req.body.password);

    var errors = req.validationErrors();
    console.log(errors);

    if (errors) {
        res.render('users/register', {user: user, errors: errors});
    } else {
        firebase.auth().createUserWithEmailAndPassword(req.body.email, req.body.password)
            .then(function(userData) {
                console.log("Authenticated user with uid: " + userData.uid);

                user.uid = userData.uid;
                var userRef = fbRef.child('users');
                userRef.push().set(user);

                req.flash('success_msg', 'Welcome '+user.first_name+' '+user.last_name);
                res.redirect('/');
            })
            .catch(function(error) {
                console.log("Error creating user: " + error.message);
                req.flash('error_msg', 'Error creating user');
                res.redirect('/');
        });
    }
});

router.post('/login', function(req, res, next) {
    // Validation
    req.checkBody('email', 'Email is required').notEmpty();
    req.checkBody('email', 'Email is not valid').isEmail();
    req.checkBody('password', 'Password is required').notEmpty();

    var errors = req.validationErrors();

    if(errors){
        res.render('users/login', {
            errors: errors
        });
    } else {
        firebase.auth().signInWithEmailAndPassword(req.body.email, req.body.password)
			.then(function(authData) {
                console.log("Authenticated user with uid: "+ authData.uid);
                req.flash('success_msg', 'You are now logged in');
                res.redirect('/albums');
            })
			.catch(function(error) {
                console.log("Login Failed: " + error.message);
                req.flash('error_msg', 'Login Failed');
                res.redirect('/users/login');
        });
    }
});

// Logout User
router.get('/logout', function(req, res){
    // Unauthenticate the client
    firebase.auth().signOut();

    req.flash('success_msg', 'You are logged out');
    res.redirect('/users/login');
});

module.exports = router;